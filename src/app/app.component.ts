import { Component, OnInit } from '@angular/core';
import {Validators, FormBuilder,FormGroup} from '@angular/forms';
import { trigger,state,style,animate,transition } from '@angular/animations';
import { ScrollToService, ScrollToConfigOptions } from '@nicky-lenaers/ngx-scroll-to';

//importing service
import { MyService} from './app.service';
import { PostService } from './services/PostService';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  {
  title = 'STYCH';
  options:any;
  msgs:any=[];
  emailPattern: any = '[a-zA-z_.0-9]+@[a-zA-Z]+[.][a-zA-Z.]+';
  

  notification(type, summary, detail) {
    this.msgs.push({ severity: type, summary: summary, detail: detail });
  }

  fg: FormGroup;
  s:MyService
 
  constructor(private fb: FormBuilder,private httpObj: PostService ) {
    this.fg = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(2)]],
      email: ['', [Validators.required, Validators.minLength(2), Validators.email,Validators.pattern(this.emailPattern)]],
      message: ['', [Validators.required, Validators.minLength(1)]]
    })

    this.options = {
      center: { lat: 28.6279842, lng: 77.3781253 },
      zoom: 12
    };

    
  }

  clickFunc(event) {
    console.log(event);
    console.log(event.tab.textLabel, "=====================");
    if (event.tab.textLabel == "HOW IT WORKS") {
          document.getElementById("how").scrollIntoView({block: 'start', behavior: 'smooth'});
    }
    else if (event.tab.textLabel == "CONTACT US") {
      document.getElementById("contact").scrollIntoView({block: 'start', behavior: 'smooth'});
    }
}

send(){
  let url='https://stych.sia.co.in/app/v1/contact_us/';
  let body={  
    name : this.fg.value.name,
    email : this.fg.value.email,
    msg : this.fg.value.message
  }
  console.log(body,"[[[[[[[[[[[[[[[[[[[[");


  if(this.fg.valid)
  {
    this.httpObj.postRequest(url, body).subscribe(data=>
      {
        //this.ms.add({severity:'error', summary
        console.log(data,"@@@@@@@@@@@@@@@    data from api       @@@@@@@@@");
        this.notification("success","Send Successfully :)","We Will Reach U Soon");
        this.fg.reset();
        //sendButton on-off
        // if(data.json.success==true){
        //   this.sendButton=true;
        // }

      },
      err=>
      {
          console.log("oops!!!! error buddy ");
          this.notification("warn","Try Again  :)","Server Request Error");
      })
  }
  else if(!this.fg.value.name && !this.fg.value.email && !this.fg.value.message )
  {
    this.notification("warn"," All Fields are Mandatory :)","");
  }
  else if(!this.fg.value.name && !this.fg.value.email  )
  {
    this.notification("warn","Name & Email are Mandatory :)","");
  }
  else if(!this.fg.value.email && !this.fg.value.message )
  {
    this.notification("warn","Email and Message are Mandatory :)","");
  }
  else if(!this.fg.value.name  && !this.fg.value.message )
  {
    this.notification("warn","Name and Message are Mandatory :)","");
  }

  else if(!this.fg.value.name){
    this.notification("error","Name is Mandatory :)","");
  }
  else if(!this.fg.value.email){
    this.notification("error","Email is Mandatory :)","");
  }
  else if(!this.fg.value.message)
  {
    this.notification("error","Message is Mandatory :)","");
  }

  else if(!this.fg.controls['name'].valid ){
    console.log(this.fg.controls['name'].valid);
    this.notification("info","Name is not Valid :)","Name must have 2 character.");
  }
  else if(!this.fg.controls['email'].valid){
    this.notification("info","Email is not Valid :)","Email is like abc@example.com");
  }
  else if(!this.fg.controls['message'].valid ){
    this.notification("info","Message is not Valid :)","Message must have atleast 2 character");
  }

  else{
    this.notification("error","Fill Accurate Details  :)","");
  }

}

  // let body = {
  //   name: this.fg.value.name,
  //   email: this.fg.value.email,
  //   message: this.fg.value.message
  // }

  // this.httpObj.postRequest(url, body).subscribe(data => {
  //   console.log(data, "successfully submited");
  // },
  //   err => {
  //     console.log("oops!!!! error buddy ");
    
  //   })

}



